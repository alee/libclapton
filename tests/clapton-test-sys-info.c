/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <libclapton-sys-info.h>

gint main (gint argc, gchar **argv)
{
  ClaptonSysInfo *info;

  info = clapton_sys_info_new ();
  g_assert (info);

  g_print ("BuildVersion = %s\n", clapton_sys_info_get_build_version (info));
  g_print ("HardwareType = %s\n", clapton_sys_info_get_hardware_type (info));
  g_print ("Hostname = %s\n", clapton_sys_info_get_host_name (info));
  g_print ("KernelVersion = %s\n", clapton_sys_info_get_kernel_version (info));
  g_print ("SDKBuildID = %s\n", clapton_sys_info_get_sdk_build_id (info));
  g_print ("Suitename = %s\n", clapton_sys_info_get_suite_name (info));
  g_print ("SystemBuildID = %s\n", clapton_sys_info_get_system_build_id (info));
  g_print ("SystemName = %s\n", clapton_sys_info_get_system_name (info));
  g_print ("SystemVersion = %s\n", clapton_sys_info_get_system_version (info));
  g_print ("Variant Name = %s\n", clapton_sys_info_get_variant_name (info));

  clapton_sys_info_free (info);
  return 0;
}
