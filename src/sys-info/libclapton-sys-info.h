/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __CLAPTON_SYSINFO_H__
#define __CLAPTON_SYSINFO_H__

#include <glib-object.h>

/**
 * SECTION:sys-info/libclapton-sys-info.h
 * @title: ClaptonSysInfo
 * @short_description: Get system information
 *
 * This API is meant to be used by update and app store services, not by
 * applications.
 * It allows such service to retrieve system informations such as the build
 * version etc.
 */

typedef struct _ClaptonSysInfo ClaptonSysInfo;

GType clapton_sys_info_get_type (void);

ClaptonSysInfo * clapton_sys_info_new (void);
void clapton_sys_info_free (ClaptonSysInfo *info);

const gchar * clapton_sys_info_get_build_version (ClaptonSysInfo *info);
const gchar * clapton_sys_info_get_hardware_type (ClaptonSysInfo *info);
const gchar * clapton_sys_info_get_host_name (ClaptonSysInfo *info);
const gchar * clapton_sys_info_get_kernel_version (ClaptonSysInfo *info);
const gchar * clapton_sys_info_get_sdk_build_id (ClaptonSysInfo *info);
const gchar * clapton_sys_info_get_suite_name (ClaptonSysInfo *info);
const gchar * clapton_sys_info_get_system_build_id (ClaptonSysInfo *info);
const gchar * clapton_sys_info_get_system_name (ClaptonSysInfo *info);
const gchar * clapton_sys_info_get_system_version (ClaptonSysInfo *info);
const gchar * clapton_sys_info_get_variant_name (ClaptonSysInfo *info);

#endif /* __CLAPTON_SYSINFO_H__ */
