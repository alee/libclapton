/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */



#include "config.h"

#include <gio/gio.h>
#include <glib.h>
#include <errno.h>
#include <sys/utsname.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <libclapton-sys-info.h>

#define BASEKERNEL "Linux"

#define SUITE_INFO_FILE "/etc/image_version"

/*
 * Syntax as on 2015 Thu Feb 19, 14:32:06 ....!!!
 * <project-name> <suite-name> <image-id> <variant> <suiteId:Major:Minor>
 *
 */

enum
{
  PROJECT_POS = 0,
  SUITE_POS,
  IMAGE_VER_POS,
  VARIANT_POS,
  APPSTORE_VER_POS,
  NUM_FIELDS
};

#define APPSTORE_SUITE_ID 1
#define APPSTORE_MAJOR_VER 2
#define APPSTORE_MINOR_VER 3

/**
 * ClaptonSysInfo:
 *
 * The opaque ClaptonSysInfo structure.
 */
struct _ClaptonSysInfo
{
  gchar *build_version;
  gchar *hardware_type;
  const gchar *host_name;
  gchar *kernel_version;
  gchar *sdk_build_id;
  gchar *suite_name;
  gchar *system_build_id;
  gchar *system_name;
  gchar *system_version;
  gchar *variant_name;
};

static gboolean
parse_version_file (const gchar *path,
                    gchar **system_name,
                    gchar **suite_name,
                    gchar **system_version,
                    gchar **variant_name,
                    gchar **build_version,
                    GError **error)
{
  GFile *f;
  GFileInputStream *in;
  GDataInputStream *data_in;
  gboolean found = FALSE;
  gchar *line;
  gchar **contents = NULL;

  f = g_file_new_for_path (path);
  in = g_file_read (f, NULL, error);

  if (!in)
    {
      g_object_unref (f);
      return FALSE;
    }

  data_in = g_data_input_stream_new (G_INPUT_STREAM (in));

  do
    {
      gsize len;
      GError *err = NULL;

      line = g_data_input_stream_read_line (data_in, &len, NULL, &err);
      if (!line)
        {
          if (err)
            {
              g_propagate_error (error, err);
            }
          else
            {
              /* EOF */
              g_set_error_literal (error, G_IO_ERROR,
                                   G_IO_ERROR_INVALID_DATA,
                                   "did not find info fields");
            }

          /* Either we failed to read the file or didn't find the fields we were
           * looking for before eaching EOF. */
          goto out;
        }

      /* Can the line hold at least all the fields? */
      if (len < 2 * NUM_FIELDS - 1)
        goto next;

      /* Skip comments */
      if (line[0] == '#')
        goto next;

      contents = g_strsplit (line, " ", -1);
      if (g_strv_length (contents) < NUM_FIELDS - 1)
        {
          g_strfreev (contents);
          contents = NULL;
          goto next;
        }

      found = TRUE;
next:
      g_free (line);
    } while (!found);

  if (system_name)
    *system_name = g_strdup (contents[PROJECT_POS]);
  if (suite_name)
    *suite_name = g_strdup (contents[SUITE_POS]);
  if (system_version)
    *system_version = g_strdup (contents[IMAGE_VER_POS]);
  if (variant_name)
    *variant_name = g_strdup (contents[VARIANT_POS]);
  if (build_version)
    *build_version = g_strdup (contents[APPSTORE_VER_POS]);

out:
  g_strfreev (contents);
  g_object_unref (f);
  g_object_unref (in);
  g_object_unref (data_in);
  return found;
}

static ClaptonSysInfo *
clapton_sys_info_copy (ClaptonSysInfo *info)
{
  ClaptonSysInfo *copy = g_slice_new0 (ClaptonSysInfo);

  copy->build_version = g_strdup (info->build_version);
  copy->hardware_type = g_strdup (info->hardware_type);
  copy->host_name = info->host_name;
  copy->kernel_version = g_strdup (info->kernel_version);
  copy->sdk_build_id = g_strdup (info->sdk_build_id);
  copy->suite_name = g_strdup (info->suite_name);
  copy->system_build_id = g_strdup (info->system_build_id);
  copy->system_name = g_strdup (info->system_name);
  copy->system_version = g_strdup (info->system_version);
  copy->variant_name = g_strdup (info->variant_name);

  return copy;
}

G_DEFINE_BOXED_TYPE (ClaptonSysInfo, clapton_sys_info, clapton_sys_info_copy,
                     clapton_sys_info_free)

/**
 * clapton_sys_info_new:
 *
 * Create a new #ClaptonSysInfo containing the information of the system.
 * Free with clapton_sys_info_free() once done.
 *
 * Returns: (transfer full): a new #ClaptonSysInfo
 */
ClaptonSysInfo * clapton_sys_info_new (void)
{
  ClaptonSysInfo *info;
  struct utsname uts;
  GError *error = NULL;

  info = g_slice_new0 (ClaptonSysInfo);

  info->host_name = g_get_host_name ();

  if (uname (&uts) == 0)
    {
      info->hardware_type = g_strdup (uts.machine);
      info->kernel_version = g_strdup_printf ("%s-%s", BASEKERNEL, uts.release);
    }
  else
    {
      g_warning ("uname() failed: %s", g_strerror (errno));
    }

  if (!parse_version_file (SUITE_INFO_FILE, &info->system_name,
                           &info->suite_name, &info->system_version,
                           &info->variant_name, &info->build_version, &error))
    {
      g_warning ("Failed to parse '%s': %s", SUITE_INFO_FILE, error->message);
      g_error_free (error);
    }

  /* To be implemented */
  info->system_build_id = NULL;
  info->sdk_build_id = NULL;

  return info;
}

/**
 * clapton_sys_info_free:
 * @info: a #ClaptonSysInfo
 *
 * Free a #ClaptonSysInfo returned by clapton_sys_info_new().
 */
void
clapton_sys_info_free (ClaptonSysInfo *info)
{
  g_return_if_fail (info);

  g_free (info->build_version);
  g_free (info->hardware_type);
  g_free (info->kernel_version);
  g_free (info->sdk_build_id);
  g_free (info->suite_name);
  g_free (info->system_build_id);
  g_free (info->system_name);
  g_free (info->system_version);
  g_free (info->variant_name);

  g_slice_free (ClaptonSysInfo, info);
}

/**
 * clapton_sys_info_get_build_version:
 * @info: a #ClaptonSysInfo
 *
 * Return the build version of the system.
 *
 * Returns: (transfer none) (nullable): the build version
 */
const gchar *
clapton_sys_info_get_build_version (ClaptonSysInfo *info)
{
  return info->build_version;
}

/**
 * clapton_sys_info_get_hardware_type:
 * @info: a #ClaptonSysInfo
 *
 * Return the hardware type of the system.
 *
 * Returns: (transfer none) (nullable): the hardware type
 */
const gchar *
clapton_sys_info_get_hardware_type (ClaptonSysInfo *info)
{
  return info->hardware_type;
}

/**
 * clapton_sys_info_get_host_name:
 * @info: a #ClaptonSysInfo
 *
 * Return the host name of the system.
 *
 * Returns: (transfer none): the host name
 */
const gchar *
clapton_sys_info_get_host_name (ClaptonSysInfo *info)
{
  return info->host_name;
}

/**
 * clapton_sys_info_get_kernel_version:
 * @info: a #ClaptonSysInfo
 *
 * Return the kernel version of the system.
 *
 * Returns: (transfer none) (nullable): the kernel version
 */
const gchar *
clapton_sys_info_get_kernel_version (ClaptonSysInfo *info)
{
  return info->kernel_version;
}

/**
 * clapton_sys_info_get_sdk_build_id:
 * @info: a #ClaptonSysInfo
 *
 * Return the SDK build ID of the system.
 *
 * Returns: (transfer none) (nullable): the SDK build ID
 */
const gchar *
clapton_sys_info_get_sdk_build_id (ClaptonSysInfo *info)
{
  return info->sdk_build_id;
}

/**
 * clapton_sys_info_get_suite_name:
 * @info: a #ClaptonSysInfo
 *
 * Return the suite name of the system.
 *
 * Returns: (transfer none) (nullable): the suite name
 */
const gchar *
clapton_sys_info_get_suite_name (ClaptonSysInfo *info)
{
  return info->suite_name;
}

/**
 * clapton_sys_info_get_system_build_id:
 * @info: a #ClaptonSysInfo
 *
 * Return the system build ID.
 *
 * Returns: (transfer none) (nullable): the system build ID
 */
const gchar *
clapton_sys_info_get_system_build_id (ClaptonSysInfo *info)
{
  return info->system_build_id;
}

/**
 * clapton_sys_info_get_system_name:
 * @info: a #ClaptonSysInfo
 *
 * Return the system name of the system.
 *
 * Returns: (transfer none) (nullable): the system name
 */
const gchar *
clapton_sys_info_get_system_name (ClaptonSysInfo *info)
{
  return info->system_name;
}

/**
 * clapton_sys_info_get_system_version:
 * @info: a #ClaptonSysInfo
 *
 * Return the version of the system
 *
 * Returns: (transfer none) (nullable): the system version
 */
const gchar *
clapton_sys_info_get_system_version (ClaptonSysInfo *info)
{
  return info->system_version;
}

/**
 * clapton_sys_info_get_variant_name:
 * @info: a #ClaptonSysInfo
 *
 * Return the variant name of the system.
 *
 * Returns: (transfer none) (nullable): the variant name
 */
const gchar *
clapton_sys_info_get_variant_name (ClaptonSysInfo *info)
{
  return info->variant_name;
}
